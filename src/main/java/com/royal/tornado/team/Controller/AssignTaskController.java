package com.royal.tornado.team.Controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.royal.tornado.team.Entity.AssignTask;
import com.royal.tornado.team.Service.AssignTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("task/assign")
public class AssignTaskController {

    @Autowired
    AssignTaskService ats;

    @PostMapping("/create")
    public String assign(@RequestBody List<AssignTask> v){
        return ats.assign(v);
    }

    
    //For getting player id wise tasks
    @GetMapping("/list")
    public List<AssignTask> list(@RequestParam("id") Long id){
        return ats.list(id);
    }

    
    //For getting schedule wise tasks
    @PostMapping("/all/list")
    public List<AssignTask> alllist(@RequestBody String data) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> artist = mapper.readValue(data, Map.class);

        LocalTime start = LocalTime.parse(artist.get("start").toString());
        LocalTime end = LocalTime.parse(artist.get("end").toString());

        return ats.alllist(start, end);
    }

}
