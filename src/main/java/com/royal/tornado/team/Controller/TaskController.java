package com.royal.tornado.team.Controller;

import com.royal.tornado.team.Entity.Task;
import com.royal.tornado.team.Service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("task")
public class TaskController {

    @Autowired
    TaskService ts;

    @PostMapping("/create")
    public String create(@RequestBody Task task){
        return ts.create(task);
    }

}
