package com.royal.tornado.team.Controller;

import com.royal.tornado.team.Entity.Player;
import com.royal.tornado.team.Service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("player")
public class PlayerController {

    @Autowired
    PlayerService ps;

    @PostMapping("/create")
    public String create(@RequestBody Player player){
        return ps.create(player);
    }


    @GetMapping("/list")
    public List<Player> list(@RequestParam("id") Long id){
        return ps.list(id);
    }
}
