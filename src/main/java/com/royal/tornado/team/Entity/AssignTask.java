package com.royal.tornado.team.Entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "assigned_tasks")
public class AssignTask {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @OneToOne(fetch=FetchType.EAGER, cascade={CascadeType.PERSIST,CascadeType.REMOVE})
    @JoinColumn(name = "task_id", referencedColumnName = "id",insertable = false,updatable = false)
    Task task;

    @Column(name = "player_id")
    Long playerId;
    @OneToOne(fetch=FetchType.EAGER, cascade={CascadeType.PERSIST,CascadeType.REMOVE})
    @JoinColumn(name = "player_id", referencedColumnName = "id",insertable = false,updatable = false)
    Player player;

    @Column(name = "task_id")
    Long taskId;

    LocalTime schedule;

    Integer status;

    @Column(name = "created_by")
    Integer createdBy;
    @Column(nullable = true, name = "updated_by")
    Integer updatedBy;
    @Column(nullable = true, name = "deleted_by")
    Integer deletedBy;
    @Column(name = "created_at", nullable = false, updatable = false)
    @CreationTimestamp
    private Date createdAt;
    @Column(name = "updated_at", nullable = true, updatable = false)
    @UpdateTimestamp
    private Date updatedAt;
    @Column(name = "deleted_at", nullable = true)
    @UpdateTimestamp
    private Date deletedAt;
}
