package com.royal.tornado.team.Entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "players")
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(nullable = false)
    String name;
    Integer age;
    String position;

    @Column(name = "jersey_no")
    Integer jerseyNo;
    Integer status;

    @Column(name = "created_by")
    Integer createdBy;
    @Column(nullable = true, name = "updated_by")
    Integer updatedBy;
    @Column(nullable = true, name = "deleted_by")
    Integer deletedBy;
    @Column(name = "created_at", nullable = false, updatable = false)
    @CreationTimestamp
    private Date createdAt;
    @Column(name = "updated_at", nullable = true, updatable = false)
    @UpdateTimestamp
    private Date updatedAt;
    @Column(name = "deleted_at", nullable = true)
    @UpdateTimestamp
    private Date deletedAt;

}
