package com.royal.tornado.team.Repository;

import com.royal.tornado.team.Entity.AssignTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalTime;
import java.util.List;

@Repository
public interface AssignTaskRepository extends JpaRepository<AssignTask, Long> {
    List<AssignTask> findByPlayerId(Long id);
    List<AssignTask> findByScheduleBetween(LocalTime start, LocalTime end);
}
