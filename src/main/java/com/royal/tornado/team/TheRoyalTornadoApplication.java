package com.royal.tornado.team;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TheRoyalTornadoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TheRoyalTornadoApplication.class, args);
	}

}
