package com.royal.tornado.team.Service;

import com.royal.tornado.team.Entity.Player;
import com.royal.tornado.team.Repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class PlayerService {

    @Autowired
    PlayerRepository pr;

    public String create(Player player) {

        if (!player.getName().isEmpty()) {
            pr.save(player);
            return "Success";
        } else
            return "Name is Mandatory";
    }

    public List<Player> list(Long id) {
        return pr.findAll();
    }
}
