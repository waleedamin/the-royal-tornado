package com.royal.tornado.team.Service;

import com.royal.tornado.team.Entity.AssignTask;
import com.royal.tornado.team.Repository.AssignTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.List;

@Service
public class AssignTaskService {

    @Autowired
    AssignTaskRepository atr;

    public String assign(List<AssignTask> v) {
        atr.saveAll(v);
        return "Success";
    }

    public List<AssignTask> list(Long id) {
        return atr.findByPlayerId(id);
    }

    public List<AssignTask> alllist(LocalTime start, LocalTime end) {
        return atr.findByScheduleBetween(start, end);
    }
}
