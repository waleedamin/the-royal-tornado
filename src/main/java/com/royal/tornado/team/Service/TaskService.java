package com.royal.tornado.team.Service;

import com.royal.tornado.team.Entity.Task;
import com.royal.tornado.team.Repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskService {

    @Autowired
    TaskRepository tr;

    public String create(Task task) {
        if (!task.getName().isEmpty()) {
            tr.save(task);
            return "Success";
        } else
            return "Name is Mandatory";
    }
}
